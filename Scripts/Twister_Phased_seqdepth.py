#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  2 09:23:26 2018

@author: devin
"""
# =============================================================================
# determines and plots the average sequencing read depth
# =============================================================================
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
Twister_05PhiX=pickle.load(open("../Pickle/Twister_unphased_0.5_relative_cleavage.p", "rb"))
Twister_25PhiX=pickle.load(open("../Pickle/Twister_phased_0.5_relative_cleavage.p", "rb"))
tw05cleaved=pickle.load(open("../Pickle/Twister_unphased_0.5_cleaved.p", "rb"))
tw05uncleaved=pickle.load(open("../Pickle/Twister_unphased_0.5_uncleaved.p", "rb"))
tw25cleaved=pickle.load(open("../Pickle/Twister_phased_0.5_cleaved.p", "rb"))
tw25uncleaved=pickle.load(open("../Pickle/Twister_phased_0.5_uncleaved.p", "rb"))

#%% Plots the cleaved and uncleaved read depth
sns.set_style('white')
sns.set_style("ticks")
data=tw25cleaved
quality_mean=[]
for seq in data:
    x=np.mean(data[seq])
    quality_mean.append(x)
    
plt.figure(1,figsize=[12,3])
plt.subplot(121)
weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(0,42,2.5)-0.35)
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('sequencing depth',weight='bold',fontsize=15,color='black')
plt.ylabel('proportion of sequences',weight='bold',fontsize=15,color='black')
plt.axvline(np.mean(quality_mean), color='darkred', linestyle='dashed', linewidth=2)
plt.bar(center, hist, align='center', width=width, color='firebrick',edgecolor='black',linewidth=2,alpha=0.9)
plt.title("cleaved reads",weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
mean25cleaved_mean=np.mean(quality_mean)
mean25cleaved_std=np.std(quality_mean)/np.sqrt(len(quality_mean))
print('mean25cleaved = ',mean25cleaved_mean, mean25cleaved_std)

data=tw05cleaved
quality_mean=[]
for seq in data:
    x=np.mean(data[seq])
    quality_mean.append(x)

weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(0,42,2.5)+0.35)
width = 0.5 * (bins[1] - bins[0])
center = ((bins[:-1] + bins[1:]) / 2)
plt.bar(center, hist, align='center', width=width, color='steelblue',edgecolor='black',linewidth=2,alpha=0.9)
plt.axvline(np.mean(quality_mean), color='navy', linestyle='dashed', linewidth=2)
plt.yticks ([0.0,0.1,0.2,0.3])
mean25cleaved_mean=np.mean(quality_mean)
mean25cleaved_std=np.std(quality_mean)/np.sqrt(len(quality_mean))
print('mean05cleaved = ',mean25cleaved_mean, mean25cleaved_std)

data=tw25uncleaved
quality_mean=[]
for seq in data:
    x=np.mean(data[seq])
    quality_mean.append(x)
    
plt.subplot(122)
weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(0,890,50)-7)
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('sequencing depth',weight='bold',fontsize=15,color='black')
#plt.ylabel('proportion of sequences',weight='bold',fontsize=15,color='black')
plt.axvline(np.mean(quality_mean), color='darkred', linestyle='dashed', linewidth=2)
plt.bar(center, hist, align='center', width=width, color='firebrick',edgecolor='black',linewidth=2,alpha=0.9)
plt.title("uncleaved reads",weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
mean25cleaved_mean=np.mean(quality_mean)
mean25cleaved_std=np.std(quality_mean)/np.sqrt(len(quality_mean))
print('mean25uncleaved = ',mean25cleaved_mean, mean25cleaved_std)

data=tw05uncleaved
quality_mean=[]
for seq in data:
    x=np.mean(data[seq])
    quality_mean.append(x)

weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(0,890,50)+7)
width = 0.5 * (bins[1] - bins[0])
center = ((bins[:-1] + bins[1:]) / 2)
plt.bar(center, hist, align='center', width=width, color='steelblue',edgecolor='black',linewidth=2,alpha=0.9)
plt.axvline(np.mean(quality_mean), color='navy', linestyle='dashed', linewidth=2)
plt.yticks ([0.0,0.1,0.2,0.3])
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig('../Figures/Twister_Phased_seqdepth.png',bbox_inches='tight',dpi=1000,transparent=True)
mean25cleaved_mean=np.mean(quality_mean)
mean25cleaved_std=np.std(quality_mean)/np.sqrt(len(quality_mean))
print('mean05uncleaved = ',mean25cleaved_mean, mean25cleaved_std)

