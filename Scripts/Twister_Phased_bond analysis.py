#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 16:58:12 2018

@author: devin
"""
# =============================================================================
# Analysis of relative activities of twister ribozymes based on the composition
# of the base pairs formed in the T1 pseudoknot
# =============================================================================
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
Twister_05PhiX=pickle.load(open("../Pickle/Twister_phased_0.5_relative_cleavage.p", "rb"))
Twister_25PhiX=pickle.load(open("../Pickle/Twister_phased_25_relative_cleavage.p", "rb"))

#%% Uploads data and determines puts each sequence into a category based on its basepairs
DATA={}
for seq in Twister_05PhiX:
    DATA[seq]=Twister_05PhiX[seq]/Twister_05PhiX['CCCGGG']

basepair={'A':'T','T':'A','G':'C','C':'G'}
GUwobble={'G':'T','T':'G'}
CAwobble={'C':'A','A':'T'}
pairs={1:6,2:5,3:4}
bonds={}
basepairIII=[]
basepairII=[]
bondpairs={'III':[],'I*I':[],'II*':[],'I**':[],'*II':[],'IIX':[],'IXI':[],'XII':[],'**I':[],'*I*':[],'IX*':[],'I*X':[],'X*I':[],'XI*':[],'IXX':[],'XXI':[],'***':[],'X**':[],'XX*':[],'*XI':[],'*IX':[],'XIX':[],'*X*':[],'XXX':[],'X*X':[],'*XX':[],'**X':[]}
for seq in DATA:
    for i in range(1,4):
        if seq[i-1] in basepair and basepair[seq[i-1]]==seq[(pairs[i])-1]:
            bonds[i]='I'
        elif seq[i-1] in GUwobble and GUwobble[seq[i-1]]==seq[(pairs[i])-1]:
            bonds[i]='*'
        #elif seq[i-1] in CAwobble and CAwobble[seq[i-1]]==seq[(pairs[i])-1]:
         #   bonds[i]='#'
        else:
            bonds[i]='X'
    x=bonds[1]+bonds[2]+bonds[3]
    bondpairs[x].append(DATA[seq])
    if x=='III':
        basepairIII.append(seq)
    if x=='*II':
        basepairII.append(seq)
    if x=='I*I':
        basepairII.append(seq)
    if x=='II*':
        basepairII.append(seq)
    if x=='III':
        basepairII.append(seq)
mean={}
sem={}
for seq in bondpairs:
    mean[seq]=np.mean(bondpairs[seq])
    sem[seq]=stats.sem(bondpairs[seq])

#%% plots bar graph
colors=[]
for i in range(0,4):
    colors.append('cornflowerblue')
for i in range(4,27):
    colors.append('salmon')

sns.set_style('ticks')
plt.figure(1,figsize=[10,4])
sns.despine( top=False, right=False, left=False, bottom=False, offset=None, trim=True)
plt.xticks([])
data=[mean,sem]
df = pd.DataFrame(data,index=['mean', 'sem'])
df = df.T
df2=df.sort_values('mean',ascending=False)
#plt.axhline(0.24,color='black',linestyle=':')
plt.bar(range(0,27),df['mean'],yerr=df['sem'],color=colors,edgecolor='black',linewidth=1)
plt.ylabel('fraction cleaved')
order=df.index
print(order)
plt.savefig('../Figures/Twister_Phased_bond_analysis.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% plots distribution of relative activities categorized by the basepairs
sns.set(font_scale=2,style='ticks')
plt.figure(2,figsize=[10,4])
plt.tick_params(axis='both',direction='inout')
df=pd.DataFrame()
df[seq]=pd.Series(bondpairs['XXX'])
for seq in bondpairs:
    x=bondpairs[seq]
    df[seq]=pd.Series(x)
order=['III', '*II', 'I*I', 'II*', 'XII', 'I**', '**I', 'IXI', 'IIX', '*X*',
       'X*I', '*XI', 'I*X', 'X**', 'XI*', 'IX*', 'XXI', 'X*X', 'XXX', 'IXX',
       'XIX', '*I*', '*XX', '*IX', 'XX*', '**X', '***']
sns.set_palette('RdBu_r',17)
sns.violinplot(data=df,order=order,bw=0.2,scale='width',inner=None,palette='Blues_r',ecolor='black',cut=0)
sns.despine(offset=10, trim=True)
plt.xticks(range(0,27),[],weight='bold',fontsize=15,color='black')
plt.yticks([0.0,0.5,1,1.5],weight='bold',fontsize=15,color='black')
x=list(df2['mean'])
plt.plot(x,'--',color='black',marker='d',mfc='darkgrey',mec='black',mew=1)
#plt.ylabel('relative fitness',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/Twister_Phased_bond_analysis_violin.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% Calculate and plot the Gibbs free energy changes for each sequence that retains
#   three basepairs based on the Nearest Neighbor Database (NNDB)
plt.figure(3)
energy={}
gc=[]
for seq in basepairIII:
    if seq[0]=='C':
        if seq[1]=='A':
            x=(-2.1)
        elif seq[1]=='C':
            x=(-3.3)
        elif seq[1]=='G':
            x=(-2.4)
        elif seq[1]=='T':
            x=(-2.1)
    if seq[0]=='G':
        if seq[1]=='A':
            x=(-2.4)
        elif seq[1]=='C':
            x=(-3.4)
        elif seq[1]=='G':
            x=(-3.3)
        elif seq[1]=='T':
            x=(-2.2)
    if seq[0]=='T':
        if seq[1]=='A':
            x=(-1.3)
        elif seq[1]=='C':
            x=(-2.4)
        elif seq[1]=='G':
            x=(-2.1)
        elif seq[1]=='T':
            x=(-0.9)
    if seq[0]=='A':
        if seq[1]=='A':
            x=(-0.9)
        elif seq[1]=='C':
            x=(-2.2)
        elif seq[1]=='G':
            x=(-2.1)
        elif seq[1]=='T':
            x=(-1.1)
    if seq[1]=='C':
        if seq[2]=='A':
            x+=(-2.1)
        elif seq[2]=='C':
            x+=(-3.3)
        elif seq[2]=='G':
            x+=(-2.4)
        elif seq[2]=='T':
            x+=(-2.1)
    if seq[1]=='G':
        if seq[2]=='A':
            x+=(-2.4)
        elif seq[2]=='C':
            x+=(-3.4)
        elif seq[2]=='G':
            x+=(-3.3)
        elif seq[2]=='T':
            x+=(-2.2)
    if seq[1]=='T':
        if seq[2]=='A':
            x+=(-1.3)
        elif seq[2]=='C':
            x+=(-2.4)
        elif seq[2]=='G':
            x+=(-2.1)
        elif seq[2]=='T':
            x+=(-0.9)
    if seq[1]=='A':
        if seq[2]=='A':
            x+=(-0.9)
        elif seq[2]=='C':
            x+=(-2.2)
        elif seq[2]=='G':
            x+=(-2.1)
        elif seq[2]=='T':
            x+=(-1.1)
    energy[seq]=x+4.09
plt.figure(figsize=[6,3])
sns.set_palette('Blues_r',7)
x=[]
y=[]
for seq in basepairIII:
    x.append(energy[seq])
    y.append(DATA[seq])
df=pd.DataFrame()
df['x']=x
df['y']=y
sns.regplot(x='x',y='y',data=df,scatter=True, fit_reg=True,scatter_kws={'edgecolor':'black','linewidth':1.5})
plt.ylabel('',weight='bold',fontsize=15,color='black')
plt.xlabel('Gibbs free energy changes (kcal/mol)',weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
sns.despine()
plt.savefig('../Figures/Twister_Phased_NNDB_analysis.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% calculate the correlation between Gibbs and relative activity
from scipy import stats
xlist=df['x'].tolist()
ylist=df['y'].tolist()
slope, intercept, r_value, p_value, std_err = stats.linregress(xlist, ylist)
print ('R^2 : ', r_value**2, p_value)
