#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 09:43:51 2020

@author: devin
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 02:13:16 2018

@author: devin
"""
# =============================================================================
# runs FASTQC for sequencing runs with and without phased nucleotide inserts 
# and with either 25% or 0.5% PhiX
# =============================================================================
import pandas as pd
import seaborn as sns
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
sns.set_style("ticks")
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

n=0
library='Twister'
rz_WT_cleaved = 'AACACTGCCAATGCCGGTNNNAAGCCCGGATAAAAGTGGANNNGGCGG'
rz_cleaved_seq = 'GGAAAAGGGCCGCCT'
weeks = 'TCGATCCGGTTCGC'


#%% extracts and plots the quality score for the data generated with Phased 25%PhiX

n=0
quality={}
for record in SeqIO.parse("../../seq_DATA/Twister_phased_25.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 
data={}
for i in range(0,150):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
datamean={}
for num in data:
    datamean[num]=np.mean(data[num])
df_P25=pd.DataFrame(data)

P25_min=[]
P25_mean=[]
for seq in quality:
    x=np.mean(quality[seq])
    P25_mean.append(x)
    y=np.min(quality[seq])
    P25_min.append(y)
#%%
n=0
quality={}
for record in SeqIO.parse("../../seq_DATA/Twister_phased_0.5.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 
data={}
for i in range(0,150):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
datamean={}
for num in data:
    datamean[num]=np.mean(data[num])
df_P05=pd.DataFrame(data)

P05_min=[]
P05_mean=[]
for seq in quality:
    x=np.mean(quality[seq])
    P05_mean.append(x)
    y=np.min(quality[seq])
    P05_min.append(y)
#%%
n=0
quality={}
for record in SeqIO.parse("../../seq_DATA/Twister_unphased_25.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 
data={}
for i in range(0,150):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
datamean={}
for num in data:
    datamean[num]=np.mean(data[num])
df_U25=pd.DataFrame(data)

U25_min=[]
U25_mean=[]
for seq in quality:
    x=np.mean(quality[seq])
    U25_mean.append(x)
    y=np.min(quality[seq])
    U25_min.append(y)
#%%
n=0
quality={}
for record in SeqIO.parse("../../seq_DATA/Twister_unphased_0.5.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 
data={}
for i in range(0,150):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
datamean={}
for num in data:
    datamean[num]=np.mean(data[num])
df_U05=pd.DataFrame(data)

U05_min=[]
U05_mean=[]
for seq in quality:
    x=np.mean(quality[seq])
    U05_mean.append(x)
    y=np.min(quality[seq])
    U05_min.append(y)
#%%
plt.figure(figsize=[12,8])
plt.subplot(411)
sns.pointplot(data=df_U05,color='#b1d1fc',ci='sd',markers='d',errwidth=1.5,join=False,scale=0.75)
plt.yticks([20,25,30,35,40],fontsize=12)
plt.ylim(20,45)
plt.xticks([1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150],[])
plt.subplot(412)
sns.pointplot(data=df_U25,color='#fe7b7c',ci='sd',markers='d',errwidth=1.5,join=False,scale=0.75)
plt.yticks([20,25,30,35,40],fontsize=12)
plt.xticks([1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150],[])
plt.ylim(20,45)
plt.subplot(413)
sns.pointplot(data=df_P05,color='#01386a',ci='sd',markers='d',errwidth=1.5,join=False,scale=0.75)
plt.yticks([20,25,30,35,40],fontsize=12)
plt.xticks([1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150],[])
plt.ylim(20,45)
plt.subplot(414)
sns.pointplot(data=df_P25,color='#840000',ci='sd',markers='d',errwidth=1.5,join=False,scale=0.75)
plt.yticks([20,25,30,35,40],fontsize=12)
plt.ylim(20,45)
plt.xticks([1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150],[1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150],fontsize=12)
plt.xlabel('position in read (bp)',weight='bold',fontsize=12)
plt.savefig('../Figures/Twister_Phased_quality.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%
plt.figure(figsize=[12,3])
sns.pointplot(data=df_U05,color='#b1d1fc',ci=None,markers='d',scale=0.75,join=False,alpha=0.75)
sns.pointplot(data=df_U25,color='#fe7b7c',ci=None,markers='d',scale=0.75,join=False,alpha=0.75)
sns.pointplot(data=df_P05,color='#01386a',ci=None,markers='d',scale=0.75,join=False,alpha=0.75)
sns.pointplot(data=df_P25,color='#840000',ci=None,markers='d',scale=0.75,join=False,alpha=0.75)
plt.xticks([0,9,19,29,39,49,59,69,79,89,99,109,119,129,139,149],[1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150],fontsize=12)
plt.yticks([32,34,36,38],fontsize=12)
plt.ylabel('quality score',weight='bold',fontsize=12)
plt.xlabel('position in read (bp)',weight='bold',fontsize=12)
plt.savefig('../Figures/Twister_Phased_quality_compare.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
x=['U05']*len(U05_mean)
data1 = pd.DataFrame(list(zip(x,U05_mean)), columns =['sample', 'count'])
x=['U25']*len(U25_mean)
data2 = pd.DataFrame(list(zip(x,U25_mean)), columns =['sample', 'count'])
x=['P05']*len(P05_mean)
data3 = pd.DataFrame(list(zip(x,P05_mean)), columns =['sample', 'count'])
x=['P25']*len(P25_mean)
data4 = pd.DataFrame(list(zip(x,P25_mean)), columns =['sample', 'count'])
data=pd.concat([data1,data2,data3,data4])

g = sns.FacetGrid(data,row='sample',hue='sample',sharex=True,aspect=6, height=1, palette=['#b1d1fc','#fe7b7c','#01386a','#840000'])
g.map(sns.kdeplot, "count", clip_on=False, shade=True, alpha=1, lw=1.5, bw=0.7,cut=0)
g.map(sns.kdeplot, "count", clip_on=False, color="w", lw=1.5, bw=0.7,cut=0)
g.map(plt.axhline, y=0, lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.5)
g.set_titles("")
g.set(yticks=[])
g.despine(bottom=True, left=True)
plt.xlabel('mean sequence quality (Phred score)',weight='bold',fontsize=12)
plt.xticks(fontsize=12)
plt.savefig('../Figures/Twister_Phased_quality_mean.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
x=['U05']*len(U05_mean)
data1 = pd.DataFrame(list(zip(x,U05_min)), columns =['sample', 'count'])
x=['U25']*len(U25_mean)
data2 = pd.DataFrame(list(zip(x,U25_min)), columns =['sample', 'count'])
x=['P05']*len(P05_mean)
data3 = pd.DataFrame(list(zip(x,P05_min)), columns =['sample', 'count'])
x=['P25']*len(P25_mean)
data4 = pd.DataFrame(list(zip(x,P25_min)), columns =['sample', 'count'])
data=pd.concat([data1,data2,data3,data4])

g = sns.FacetGrid(data,row='sample',hue='sample',aspect=6, height=1, palette=['#b1d1fc','#fe7b7c','#01386a','#840000'])
g.map(sns.kdeplot, "count",clip_on=False, shade=True, alpha=1, lw=1.5, bw='scott',cut=0)
g.map(sns.kdeplot, "count",clip_on=False, color="w", lw=1.5, bw='scott',cut=0)
g.map(plt.axhline, y=0,lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.5)
g.set_titles("")
g.set(yticks=[])
g.despine(bottom=True, left=True)
plt.xlabel('min sequence quality (Phred score)',weight='bold',fontsize=12)
plt.xticks(fontsize=12)
plt.savefig('../Figures/Twister_Phased_quality_min.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
print('U05_mean',np.mean(U05_mean),np.std(U05_mean))
print('U25_mean',np.mean(U25_mean),np.std(U25_mean))
print('P05_mean',np.mean(P05_mean),np.std(P05_mean))
print('P25_mean',np.mean(P25_mean),np.std(P25_mean))

print('U05_min',np.mean(U05_min),np.std(U05_min))
print('U25_min',np.mean(U25_min),np.std(U25_min))
print('P05_min',np.mean(P05_min),np.std(P05_min))
print('P25_min',np.mean(P25_min),np.std(P25_min))

