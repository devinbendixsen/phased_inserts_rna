#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 02:13:16 2018

@author: devin
"""
# =============================================================================
# runs FASTQC for failed HDV-LIG sequencing run without phased nucleotide inserts
# =============================================================================
import pandas as pd
import seaborn as sns
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np

#%% Extracts the phred quality score from the raw sequencing data
plt.figure(1)
plt.figure(figsize=[12,2.5])
n=0
quality={}
for record in SeqIO.parse("../../../../../../../../Volumes/RNA/Raw_Seq_DATA/HDV-LIG_14/Failed_Run_1/HDV2.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 
data={}
for i in range(0,151):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])

datamean={}
for num in data:
    datamean[num]=np.mean(data[num])

#%% Plots the distribution of quality scores
import matplotlib.cm as cm

color={}
for num in datamean:
    color[num]=((datamean[num]-20)/20)
colorlist=[]
for num in color:
    colorlist.append(cm.Purples(color[num]))

plt.figure(figsize=[16,3])
df=pd.DataFrame(data)
sns.set_style('white')
sns.set_style("ticks")
sns.boxplot(data=df,fliersize=0,linewidth=2,palette=colorlist)
plt.ylabel('quality score',weight='bold',fontsize=25)
plt.xlabel('position in read (bp)',weight='bold',fontsize=25)
plt.yticks([0,10,20,30,40],[0,10,20,30,40],weight='bold',fontsize=25)
plt.xticks([0,25,50,75,100,125,150],[0,25,50,75,100,125,150],weight='bold',fontsize=25)
plt.savefig('../Figures/Twister_Phased_FASTQC_HDV-LIG.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% Determines and plots the distribution of the mean sequence quality
quality_mean=[]
for seq in quality:
    x=np.mean(quality[seq])
    quality_mean.append(x)


plt.figure(2,figsize=[6,3])
weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(14,40,1.8))
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('mean sequence quality (Phred score)',weight='bold',fontsize=15,color='black')
plt.axvline(20, color='black', linestyle='dashed', linewidth=2)
plt.axvline(28, color='black', linestyle='dashed', linewidth=2)
plt.bar(center, hist, align='center', width=width, color='indigo',edgecolor='black',linewidth=2)
plt.title("No Phased Insert + 10% PhiX",weight='bold',fontsize=15,color='indigo')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.tight_layout()
mean25cleaved_mean=np.mean(quality_mean)
mean25cleaved_std=np.std(quality_mean)/np.sqrt(len(quality_mean))
print('mean25 = ',mean25cleaved_mean, mean25cleaved_std)
plt.ylabel('proportion of sequences',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/Twister_Phased_qual_hist_HDV-LIG.png',bbox_inches='tight',dpi=1000,transparent=True)
