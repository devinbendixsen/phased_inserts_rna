#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 02:13:16 2018

@author: devin
"""
# =============================================================================
# runs FASTQC for sequencing runs with 25% and 0.5% phased nucleotide inserts
# =============================================================================
import pandas as pd
import seaborn as sns
from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np
n=0
library='Twister'
rz_WT_cleaved = 'AACACTGCCAATGCCGGTNNNAAGCCCGGATAAAAGTGGANNNGGCGG'
rz_cleaved_seq = 'GGAAAAGGGCCGCCT'
weeks = 'TCGATCCGGTTCGC'
sns.set_style('white')
sns.set_style("ticks")

#%% extracts and plots the quality score for the data generated with 25%PhiX

n=0
quality={}
for record in SeqIO.parse("../../seq_DATA/Twister_phased_25.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 
import matplotlib.cm as cm


plt.figure(1,figsize=[12,6])
plt.subplot(212)
data={}
for i in range(0,151):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
datamean={}
for num in data:
    datamean[num]=np.mean(data[num])
color={}
for num in datamean:
    color[num]=((datamean[num]-20)/20)
colorlist=[]
for num in color:
    colorlist.append(cm.Reds(color[num]))
df=pd.DataFrame(data)
sns.boxplot(data=df,fliersize=0,linewidth=2,palette=colorlist)
plt.yticks([28,32,36,40],[28,32,36,40],weight='bold',fontsize=15)
plt.xticks([0,25,50,75,100,125,150],[0,25,50,75,100,125,150],weight='bold',fontsize=15)
plt.axhline(28,color='black',linewidth=2)
plt.axhline(20,color='black',linewidth=2)
plt.ylim(28,40)

quality_mean=[]
quality_min=[]
for seq in quality:
    x=np.mean(quality[seq])
    y=np.min(quality[seq])
    quality_mean.append(x)
    quality_min.append(y)

plt.figure(2,figsize=[12,3])
plt.subplot(121)
weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(10,40,1.8)-.25)
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('mean sequence quality (Phred score)',weight='bold',fontsize=15,color='black')
plt.bar(center, hist, align='center', width=width, color='firebrick',edgecolor='black',linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.ylim(0,0.75)

plt.subplot(122)
weights = np.ones_like(quality_min)/len(quality_min)
hist, bins= np.histogram(quality_min,weights=weights,bins=np.arange(10,40,1.65)-.25)
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('min sequence quality (Phred score)',weight='bold',fontsize=15,color='black')
plt.bar(center, hist, align='center', width=width, color='firebrick',edgecolor='black',linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.ylim(0,0.75)

#%% extracts and plots the quality score for the data generated with 0.5%PhiX
quality={}
for record in SeqIO.parse("/Volumes/Research/EricHayden/Common/RNA/Raw_Seq_DATA/Twister_Phased/Phased_0.5%PhiX/Raw_DATA/Twister_phased_0.5.fastq", "fastq"):
    if n>=0:
        x=record.letter_annotations["phred_quality"]
        quality[n]=x
        n+=1
    else:
        break 

data={}
for i in range(0,151):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
datamean={}
for num in data:
    datamean[num]=np.mean(data[num])
color={}
for num in datamean:
    color[num]=((datamean[num]-20)/20)
colorlist=[]
for num in color:
    colorlist.append(cm.Blues(color[num]))


data={}
hue={}
plt.figure(1)
plt.subplot(211)
for i in range(0,151):
    data[i]=[]
    for seq in quality:
        data[i].append(quality[seq][i])
df=pd.DataFrame(data)
sns.boxplot(data=df,fliersize=0,linewidth=2,palette=colorlist)
plt.yticks([28,32,36,40],[28,32,36,40],weight='bold',fontsize=15)
plt.ylim(28,40)
plt.xticks([0,20,40,60,80],[],weight='bold',fontsize=15)
plt.axhline(28,color='black',linewidth=2)
plt.axhline(20,color='black',linewidth=2)
plt.savefig('../Figures/Twister_Phased_quality.png',bbox_inches='tight',dpi=1000,transparent=True)

quality_min=[]
quality_mean=[]
for seq in quality:
    x=np.mean(quality[seq])
    quality_mean.append(x)
    y=np.min(quality[seq])
    quality_min.append(y)

#%%
plt.figure(2,figsize=[12,3])
plt.subplot(121)
weights = np.ones_like(quality_mean)/len(quality_mean)
hist, bins= np.histogram(quality_mean,weights=weights,bins=np.arange(10,40,1.8)+.25)
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('mean sequence quality (Phred score)',weight='bold',fontsize=15,color='black')
plt.ylabel('proportion of sequences',weight='bold',fontsize=15,color='black')
plt.bar(center, hist, align='center', width=width, color='steelblue',edgecolor='black',linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.ylim(0,0.75)

plt.subplot(122)
weights = np.ones_like(quality_min)/len(quality_min)
hist, bins= np.histogram(quality_min,weights=weights,bins=np.arange(10,40,1.65)+.25)
width = 0.5 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.xlabel('min sequence quality (Phred score)',weight='bold',fontsize=15,color='black')
#plt.ylabel('proportion of sequences',weight='bold',fontsize=15,color='black')
plt.bar(center, hist, align='center', width=width, color='steelblue',edgecolor='black',linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.ylim(0,0.75)
plt.tight_layout()
plt.savefig('../Figures/Twister_Phased_qual_hist.png',bbox_inches='tight',dpi=1000,transparent=True)
