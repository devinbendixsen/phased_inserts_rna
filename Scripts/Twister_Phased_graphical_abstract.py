#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 00:47:10 2018

@author: devin
"""
# =============================================================================
# plot what a theoretical sequencing plate would look like for graphical abstract
# =============================================================================

import matplotlib.pyplot as plt
import numpy as np


N = 1000
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'red'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])
plt.savefig('../Figures/Twister_Phased_control.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
plt.figure(2)
N = 810
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'red'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])

N = 60
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'green'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])

N = 60
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'blue'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])

N = 60
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'yellow'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])
plt.savefig('../Figures/Twister_Phased_25%PhiX.png',bbox_inches='tight',dpi=1000,transparent=True)
#%%
plt.figure(3)
N = 250
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'red'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])

N = 250
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'green'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])

N = 250
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'blue'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])

N = 250
x = np.random.rand(N)
y = np.random.rand(N)
colors = np.random.rand(N)
t = 'yellow'
for seq in x:
    plt.scatter(x, y, c=t, linewidth=0.5,edgecolor='black',marker='.',s=100)
plt.xticks([])
plt.yticks([])
plt.savefig('../Figures/Twister_Phased_Phased.png',bbox_inches='tight',dpi=1000,transparent=True)