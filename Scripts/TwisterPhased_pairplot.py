#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 12:34:57 2019

@author: devin
"""

# =============================================================================
# code to determine the correlation of read counts between using 25% and 0.5%PhiX
# =============================================================================
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from scipy.stats import pearsonr

sns.set(font_scale=2, font='bold',style='ticks')
sns.set_style('ticks')
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)
    
#%%
def hide_current_axis(*args, **kwds):
    plt.gca().set_visible(False)

#%%
Twister_phased_05=pickle.load(open("../Pickle/Twister_phased_0.5_cleaved.p", "rb"))
Twister_phased_25=pickle.load(open("../Pickle/Twister_phased_25_cleaved.p", "rb"))
Twister_unphased_05=pickle.load(open("../Pickle/Twister_unphased_0.5_cleaved.p", "rb"))
Twister_unphased_25=pickle.load(open("../Pickle/Twister_unphased_25_cleaved.p", "rb"))

data=pd.DataFrame()
data['U05']=pd.Series(Twister_unphased_05)
data['U25']=pd.Series(Twister_unphased_25)
data['P05']=pd.Series(Twister_phased_05)
data['P25']=pd.Series(Twister_phased_25)
plt.figure(figsize=[4,4])
g=sns.pairplot(data,diag_kws=dict(color='w'),diag_kind='kde',plot_kws=dict(color='gray',s=25,linewidth=0.5,alpha=0.5),markers='h')
g.map_upper(hide_current_axis)
plt.savefig('../Figures/Twister_Phased_cleaved_correlation.svg',bbox_inches='tight',dpi=1000)

print('cleaved reads')
print('U05:U25',(data['U05'].corr(data['U25']))**2)
print('U05:P05',(data['U05'].corr(data['P05']))**2)
print('U05:P25',(data['U05'].corr(data['P25']))**2)
print('U25:P05',(data['U25'].corr(data['P05']))**2)
print('U25:P25',(data['U25'].corr(data['P25']))**2)
print('P05:P25',(data['P05'].corr(data['P25']))**2)

Twister_phased_05=pickle.load(open("../Pickle/Twister_phased_0.5_uncleaved.p", "rb"))
Twister_phased_25=pickle.load(open("../Pickle/Twister_phased_25_uncleaved.p", "rb"))
Twister_unphased_05=pickle.load(open("../Pickle/Twister_unphased_0.5_uncleaved.p", "rb"))
Twister_unphased_25=pickle.load(open("../Pickle/Twister_unphased_25_uncleaved.p", "rb"))

data=pd.DataFrame()
data['U05']=pd.Series(Twister_unphased_05)
data['U25']=pd.Series(Twister_unphased_25)
data['P05']=pd.Series(Twister_phased_05)
data['P25']=pd.Series(Twister_phased_25)
plt.figure(figsize=[4,4])
g=sns.pairplot(data,diag_kws=dict(color='w'),diag_kind='kde',plot_kws=dict(color='gray',s=25,linewidth=0.5,alpha=0.5),markers='h')
g.map_upper(hide_current_axis)
plt.savefig('../Figures/Twister_Phased_uncleaved_correlation.svg',bbox_inches='tight',dpi=1000)

print('uncleaved reads')
print('U05:U25',(data['U05'].corr(data['U25']))**2)
print('U05:P05',(data['U05'].corr(data['P05']))**2)
print('U05:P25',(data['U05'].corr(data['P25']))**2)
print('U25:P05',(data['U25'].corr(data['P05']))**2)
print('U25:P25',(data['U25'].corr(data['P25']))**2)
print('P05:P25',(data['P05'].corr(data['P25']))**2)

Twister_phased_05=pickle.load(open("../Pickle/Twister_phased_0.5_relative_cleavage.p", "rb"))
Twister_phased_25=pickle.load(open("../Pickle/Twister_phased_25_relative_cleavage.p", "rb"))
Twister_unphased_05=pickle.load(open("../Pickle/Twister_unphased_0.5_relative_cleavage.p", "rb"))
Twister_unphased_25=pickle.load(open("../Pickle/Twister_unphased_25_relative_cleavage.p", "rb"))

data=pd.DataFrame()
data['U05']=pd.Series(Twister_unphased_05)
data['U25']=pd.Series(Twister_unphased_25)
data['P05']=pd.Series(Twister_phased_05)
data['P25']=pd.Series(Twister_phased_25)
plt.figure(figsize=[4,4])
g=sns.pairplot(data,diag_kws=dict(color='w'),diag_kind='kde',plot_kws=dict(color='gray',s=25,linewidth=0.5,alpha=0.5),markers='h')
g.map_upper(hide_current_axis)
plt.savefig('../Figures/Twister_Phased_fraction_cleaved_correlation.svg',bbox_inches='tight',dpi=1000)

print('fraction cleaved')
print('U05:U25',(data['U05'].corr(data['U25']))**2)
print('U05:P05',(data['U05'].corr(data['P05']))**2)
print('U05:P25',(data['U05'].corr(data['P25']))**2)
print('U25:P05',(data['U25'].corr(data['P05']))**2)
print('U25:P25',(data['U25'].corr(data['P25']))**2)
print('P05:P25',(data['P05'].corr(data['P25']))**2)


