#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 14:56:10 2018

@author: devin
"""
# =============================================================================
# to compare the results of the Twister ribozyme analyses to the published Kobori et al. data.
# =============================================================================
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)

P05=pickle.load(open("../Pickle/Twister_phased_0.5_relative_cleavage.p", "rb"))
P25=pickle.load(open("../Pickle/Twister_phased_25_relative_cleavage.p", "rb"))
U05=pickle.load(open("../Pickle/Twister_unphased_0.5_relative_cleavage.p", "rb"))
U25=pickle.load(open("../Pickle/Twister_unphased_25_relative_cleavage.p", "rb"))

#%%
x=pd.read_excel('../Data/Twister_Kobori_heatmap.xlsx',sheet_name='Data')
Kobori=x.set_index('genotype')['Kobori'].to_dict()
for seq in Kobori:
    Kobori[seq]=Kobori[seq]*0.82

library='U05'
DATA=U05
vmax=0.5
#%%
def mutcalc(start,end): #determines the number of mutations and verifies that they are in the correct location
    mut=[] #number of mutations from WT
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut.append(i)
        mutnum=len(mut)
    return mutnum,mut
#%%
rz_WT_cleaved='CCCGGG'
range1=[4,5,6]
range2=[1,2,3]
mutfit={}
mut_sing1={}
mut_sing2={}

for seq in DATA:
    mutnum,mut=mutcalc(seq,rz_WT_cleaved)
    if mutnum==1 and (mut[0] in range1):
        mut_sing1['{}{}_single'.format(mut[0],seq[mut[0]-1])]=DATA[seq]
    if mutnum==1 and (mut[0] in range2):
        mut_sing2['single_{}{}'.format(mut[0],seq[mut[0]-1])]=DATA[seq]
    if mutnum==2 and (mut[0] in range1) and (mut[1] in range2):
        mutfit['{}{}_{}{}'.format(mut[0],seq[mut[0]-1],mut[1],seq[mut[1]-1])]=DATA[seq]
    if mutnum==2 and (mut[1] in range1) and (mut[0] in range2):
        mutfit['{}{}_{}{}'.format(mut[1],seq[mut[1]-1],mut[0],seq[mut[0]-1])]=DATA[seq]

#%%
mut_options={'T':['G','C','A'], 'G':['T','C','A'],'C':['T','G','A'],'A':['T','G','C']}
label1=[]
for i in range(range1[0],range1[-1]+1):
    for num in range(0,3):
        label1.append('{}{}'.format(i,mut_options[rz_WT_cleaved[i-1]][num]))
label2=[]
for i in range(range2[0],range2[-1]+1):
    for num in range(0,3):
        label2.append('{}{}'.format(i,mut_options[rz_WT_cleaved[i-1]][num]))
df=pd.DataFrame(columns=label1,index=label2)

#%%
for seq in mutfit:
    x,y=seq.split('_')
    df.at[y,x]=mutfit[seq]

#%%

plt.figure(figsize=[3,3])

df.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(data=df,cmap='GnBu',square=True,cbar=False,vmin=0,vmax=vmax)
plt.yticks([],[])
plt.xticks([],[])
plt.savefig('../Figures/Twister_Phased_{}_matrix_inset.png'.format(library),bbox_inches='tight',dpi=1000,transparent=True)

#%%

mut_options={'T':['G','C','A'], 'G':['T','C','A'],'C':['T','G','A'],'A':['T','G','C']}
label1=[]
for i in range(range1[0],range1[-1]+1):
    for num in range(0,3):
        label1.append('{}{}'.format(i,mut_options[rz_WT_cleaved[i-1]][num]))
label2=['single']
df=pd.DataFrame(columns=label1,index=label2)
for seq in mut_sing1:
    x,y=seq.split('_')
    df.at[y,x]=mut_sing1[seq]

plt.figure(figsize=[3,(3/9)])
df.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(data=df,cmap='GnBu',square=True,cbar=False,vmin=0,vmax=vmax)
plt.yticks([])
plt.xticks([])
plt.savefig('../Figures/Twister_Phased_{}_matrix_x_single.png'.format(library),bbox_inches='tight',dpi=1000,transparent=True)

#%%
mut_options={'T':['G','C','A'], 'G':['T','C','A'],'C':['T','G','A'],'A':['T','G','C']}
label2=[]
for i in range(range2[0],range2[-1]+1):
    for num in range(0,3):
        label2.append('{}{}'.format(i,mut_options[rz_WT_cleaved[i-1]][num]))
label1=['single']
df=pd.DataFrame(columns=label1,index=label2)
for seq in mut_sing2:
    x,y=seq.split('_')
    df.at[y,x]=mut_sing2[seq]

plt.figure(figsize=[3/9,(3)])
df.fillna(value=np.nan, inplace=True)
ax=sns.heatmap(data=df,cmap='GnBu',square=True,cbar=False,vmin=0,vmax=vmax)
plt.yticks([])
plt.xticks([])
plt.savefig('../Figures/Twister_Phased_{}_matrix_y_single.png'.format(library),bbox_inches='tight',dpi=1000,transparent=True)

#%%
data=pd.DataFrame()
data['Kobori']=pd.Series(Kobori)
data['U05']=pd.Series(U05)
data['U25']=pd.Series(U25)
data['P05']=pd.Series(P05)
data['P25']=pd.Series(P25)
sns.set(style='ticks', font_scale=1.75)
plt.figure(figsize=[2.5,2.5])
g = sns.JointGrid(data=data, x='U05', y='Kobori', height=5,xlim=(0,0.5),ylim=(0,1))
g = g.plot_joint(sns.regplot, color="#b1d1fc")
g = g.plot_marginals(sns.distplot, kde=False, bins=12, color="xkcd:gray")
g = g.set_axis_labels(xlabel="",ylabel="")
plt.savefig('../Figures/Twister_Phased_U05_kobori_corr.png',bbox_inches='tight',dpi=1000,transparent=True)
    
plt.figure(figsize=[2.5,2.5])
g = sns.JointGrid(data=data, x='U25', y='Kobori', height=5,xlim=(0,0.5),ylim=(0,1))
g = g.plot_joint(sns.regplot, color="#fe7b7c")
g = g.plot_marginals(sns.distplot, kde=False, bins=12, color="xkcd:gray")
g = g.set_axis_labels(xlabel="",ylabel="")
plt.savefig('../Figures/Twister_Phased_U25_kobori_corr.png',bbox_inches='tight',dpi=1000,transparent=True)
  
plt.figure(figsize=[2.5,2.5])
g = sns.JointGrid(data=data, x='P05', y='Kobori', height=5,xlim=(0,0.5),ylim=(0,1))
g = g.plot_joint(sns.regplot, color="#01386a")
g = g.plot_marginals(sns.distplot, kde=False, bins=12, color="xkcd:gray")
g = g.set_axis_labels(xlabel="",ylabel="")
plt.savefig('../Figures/Twister_Phased_P05_kobori_corr.png',bbox_inches='tight',dpi=1000,transparent=True)
  
plt.figure(figsize=[2.5,2.5])
g = sns.JointGrid(data=data, x='P25', y='Kobori', height=5,xlim=(0,0.5),ylim=(0,1))
g = g.plot_joint(sns.regplot, color="#840000")
g = g.plot_marginals(sns.distplot, kde=False, bins=12, color="xkcd:gray")
g = g.set_axis_labels(xlabel="",ylabel="")
plt.savefig('../Figures/Twister_Phased_P25_kobori_corr.png',bbox_inches='tight',dpi=1000,transparent=True)
  
#%%
print(data.corr(method='pearson'))
