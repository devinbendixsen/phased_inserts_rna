#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 31 14:04:49 2019

@author: devin
"""


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

sns.set_style("ticks")
#%%
Twister_phased_05=pickle.load(open("../Pickle/Twister_phased_0.5_cleaved.p", "rb"))
Twister_phased_25=pickle.load(open("../Pickle/Twister_phased_25_cleaved.p", "rb"))
Twister_unphased_05=pickle.load(open("../Pickle/Twister_unphased_0.5_cleaved.p", "rb"))
Twister_unphased_25=pickle.load(open("../Pickle/Twister_unphased_25_cleaved.p", "rb"))
x=['P05']*len(Twister_phased_05.values())
data1 = pd.DataFrame(list(zip(x,Twister_phased_05.values())), columns =['sample', 'count']) 
x=['P25']*len(Twister_phased_25.values())
data2 = pd.DataFrame(list(zip(x,Twister_phased_25.values())), columns =['sample', 'count'])
x=['U05']*len(Twister_unphased_05.values())
data3 = pd.DataFrame(list(zip(x,Twister_unphased_05.values())), columns =['sample', 'count'])
x=['U25']*len(Twister_unphased_25.values())
data4 = pd.DataFrame(list(zip(x,Twister_unphased_25.values())), columns =['sample', 'count'])
data=pd.concat([data3,data4,data1,data2])

g = sns.FacetGrid(data,row='sample',hue='sample',aspect=4, height=1.2, palette=['#b1d1fc','#fe7b7c','#01386a','#840000'])
g.map(sns.kdeplot, "count", clip_on=False, shade=True, alpha=1, lw=1.5, bw='scott',cut=0)
g.map(sns.kdeplot, "count", clip_on=False, color="w", lw=1.5, bw='scott',cut=0)
g.map(plt.axhline, y=0, lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.5)
g.set_titles("")
g.set(yticks=[],xticks=[0,25,50])
g.despine(bottom=True, left=True)
plt.xticks(fontsize=18)
plt.xlabel('sequencing depth',weight='bold',fontsize=20)
plt.savefig('../Figures/Twister_Phased_read-depth_cleaved.png',bbox_inches='tight',dpi=1000,transparent=True)

print('P05',sum(Twister_phased_05.values())/4096)
print('P25',sum(Twister_phased_25.values())/4096)
print('U05',sum(Twister_unphased_05.values())/4096)
print('U25',sum(Twister_unphased_25.values())/4096)

#%%
Twister_phased_05=pickle.load(open("../Pickle/Twister_phased_0.5_uncleaved.p", "rb"))
Twister_phased_25=pickle.load(open("../Pickle/Twister_phased_25_uncleaved.p", "rb"))
Twister_unphased_05=pickle.load(open("../Pickle/Twister_unphased_0.5_uncleaved.p", "rb"))
Twister_unphased_25=pickle.load(open("../Pickle/Twister_unphased_25_uncleaved.p", "rb"))
x=['P05']*len(Twister_phased_05.values())
data1 = pd.DataFrame(list(zip(x,Twister_phased_05.values())), columns =['sample', 'count']) 
x=['P25']*len(Twister_phased_25.values())
data2 = pd.DataFrame(list(zip(x,Twister_phased_25.values())), columns =['sample', 'count'])
x=['U05']*len(Twister_unphased_05.values())
data3 = pd.DataFrame(list(zip(x,Twister_unphased_05.values())), columns =['sample', 'count'])
x=['U25']*len(Twister_unphased_25.values())
data4 = pd.DataFrame(list(zip(x,Twister_unphased_25.values())), columns =['sample', 'count'])
data=pd.concat([data3,data4,data1,data2])

g = sns.FacetGrid(data,row='sample',hue='sample',aspect=4, height=1.2, palette=['#b1d1fc','#fe7b7c','#01386a','#840000'])
g.map(sns.kdeplot, "count", clip_on=False, shade=True, alpha=1, lw=1.5, bw='scott',cut=0)
g.map(sns.kdeplot, "count", clip_on=False, color="w", lw=1.5, bw='scott',cut=0)
g.map(plt.axhline, y=0, lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.5)
g.set_titles("")
g.set(yticks=[],xticks=[0,250,500,750])
g.despine(bottom=True, left=True)
plt.xticks(fontsize=18)
plt.xlabel('sequencing depth',weight='bold',fontsize=20)
plt.savefig('../Figures/Twister_Phased_read-depth_uncleaved.png',bbox_inches='tight',dpi=1000,transparent=True)

print('P05',sum(Twister_phased_05.values())/4096)
print('P25',sum(Twister_phased_25.values())/4096)
print('U05',sum(Twister_unphased_05.values())/4096)
print('U25',sum(Twister_unphased_25.values())/4096)
#%%
Twister_phased_05=pickle.load(open("../Pickle/Twister_phased_0.5_relative_cleavage.p", "rb"))
Twister_phased_25=pickle.load(open("../Pickle/Twister_phased_25_relative_cleavage.p", "rb"))
Twister_unphased_05=pickle.load(open("../Pickle/Twister_unphased_0.5_relative_cleavage.p", "rb"))
Twister_unphased_25=pickle.load(open("../Pickle/Twister_unphased_25_relative_cleavage.p", "rb"))

x=['P05']*len(Twister_phased_05.values())
data1 = pd.DataFrame(list(zip(x,Twister_phased_05.values())), columns =['sample', 'count']) 
x=['P25']*len(Twister_phased_25.values())
data2 = pd.DataFrame(list(zip(x,Twister_phased_25.values())), columns =['sample', 'count'])
x=['U05']*len(Twister_unphased_05.values())
data3 = pd.DataFrame(list(zip(x,Twister_unphased_05.values())), columns =['sample', 'count'])
x=['U25']*len(Twister_unphased_25.values())
data4 = pd.DataFrame(list(zip(x,Twister_unphased_25.values())), columns =['sample', 'count'])
data=pd.concat([data3,data4,data1,data2])

g = sns.FacetGrid(data,row='sample',hue='sample',aspect=4, height=1.2, palette=['#b1d1fc','#fe7b7c','#01386a','#840000'])
g.map(sns.kdeplot, "count", clip_on=False, shade=True, alpha=1, lw=1.5, bw='scott',cut=0)
g.map(sns.kdeplot, "count", clip_on=False, color="w", lw=1.5, bw='scott',cut=0)
g.map(plt.axhline, y=0, lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.5)
g.set_titles("")
g.set(yticks=[],xticks=[0.00,0.25,0.50])
g.despine(bottom=True, left=True)
plt.xticks(fontsize=18)
plt.xlabel('fraction cleaved',weight='bold',fontsize=20)
plt.savefig('../Figures/Twister_Phased_read-depth_fraction_cleaved.png',bbox_inches='tight',dpi=1000,transparent=True)

print('mean')
print('P05',np.mean(list(Twister_phased_05.values())))
print('P25',np.mean(list(Twister_phased_25.values())))
print('U05',np.mean(list(Twister_unphased_05.values())))
print('U25',np.mean(list(Twister_unphased_25.values())))

print('min')
print('P05',np.min(list(Twister_phased_05.values())))
print('P25',np.min(list(Twister_phased_25.values())))
print('U05',np.min(list(Twister_unphased_05.values())))
print('U25',np.min(list(Twister_unphased_25.values())))

print('max')
print('P05',np.max(list(Twister_phased_05.values())))
print('P25',np.max(list(Twister_phased_25.values())))
print('U05',np.max(list(Twister_unphased_05.values())))
print('U25',np.max(list(Twister_unphased_25.values())))


