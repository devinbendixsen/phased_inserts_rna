#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 21:17:06 2017

@author: devin
"""
# =============================================================================
# Master code to analyze HDV_N7 high throughput sequencing data.
# GOALS - To generate relative fitness measurements for each genotype.
# =============================================================================
#Import needed packages
import pickle

#%%
#Define initial variables
library='Twister_unphased_0.5'
rz_WT_cleaved = 'AACACTGCCAATGCCGGTCCCAAGCCCGGATAAAAGTGGAGGGGGCGG'
rz_cleaved_seq = 'GGAAAAGGGCCGCCT'
weeks = 'TCGATCCGGTTCGC'
mutations=[19,20,21,41,42,43] #location of expected mutations

#%%
##Identify where the data is stored.
seqDATA={}
#seqDATA[1]="../../../../../../../../Volumes/RNA/Raw_Seq_DATA/Twister_Phased/Phased_25%PhiX/Raw_DATA/Twister_phased_25.fastq"
seqDATA[1]="../../seq_DATA/{}.fastq".format(library)
#%%
#Load functions needed
def mutcalc(start,end,mutations): #determines the number of mutations and verifies that they are in the correct location
    mut=0 #number of mutations from WT
    num_mut=0 #number of mutations in the correct spot
    for i in range(1,len(end)+1):
        if start[i-1:i]!=end[i-1:i]:
            mut+=1
            if i in mutations:
                num_mut+=1
    return mut, num_mut

def slicer(sequence,mutations): #extracts the mutations located in mutations (list)
    sequence=sequence[mutations[0]-1]+sequence[mutations[1]-1]+sequence[mutations[2]-1]+sequence[mutations[3]-1]+sequence[mutations[4]-1]+sequence[mutations[5]-1]
    return sequence

#%%
num=0
DATA={}
for i in range(1,2):#builds a nested dictionary DATA with rz_cleaved, rz_uncleaved and relative_cleavage for each replicate
    DATA["rz_cleaved{}".format(i)] = {}
    DATA["rz_uncleaved{}".format(i)] = {}
    DATA["relative_cleavage{}".format(i)] = {}
    n=0
    for sequence in open (seqDATA[i]): #opens fastq sequencing files and extracts the 2nd line (sequence)
        n+=1
        if n==2:
            n=-2
            num+=1
            if weeks in sequence and len(sequence)>=(len(rz_WT_cleaved)+len(rz_cleaved_seq)+1) and 'CTATAGGA' not in sequence: #verifies that the weeks linker is in the sequence and determines that the length is long enough and the TATA box is not in it
                sequence=sequence.split(weeks,1)[0] #extracts the sequence upstream of the weeks linker
                sequence=sequence[-(len(rz_WT_cleaved)+len(rz_cleaved_seq)):] #extracts the length of the sequence and potential cleaved
                mut,num_mut=mutcalc(sequence[len(sequence)-len(rz_WT_cleaved):],rz_WT_cleaved,mutations)
                if mut<=len(mutations) and num_mut==mut: #uses mutcalc to determine that there are the correct number of mutations and they are in the correct location
                    if rz_cleaved_seq in sequence: #determines if it is an uncleaved sequence
                        sequence=sequence.split(rz_cleaved_seq,1)[1] #extracts sequence downstream of rz_cleaved_seq
                        sequence=slicer(sequence,mutations) #uses slicer to extract the mutational nucleotides
                        if sequence not in DATA["rz_uncleaved{}".format(i)]: #counts uncleaved sequences for each genotype
                            DATA["rz_uncleaved{}".format(i)][sequence]=1
                        else:
                            DATA["rz_uncleaved{}".format(i)][sequence]+=1
                    elif rz_cleaved_seq not in sequence: #determines if it is a cleaved sequence
                        sequence=sequence[-(len(rz_WT_cleaved)):] #extracts the full length sequence
                        sequence=slicer(sequence,mutations) #uses slicer to extract the mutational nucleotides
                        if sequence not in DATA["rz_cleaved{}".format(i)]: #counts cleaved sequences for each genotype
                            DATA["rz_cleaved{}".format(i)][sequence]=1
                        else:
                            DATA["rz_cleaved{}".format(i)][sequence]+=1
    for sequence in DATA["rz_uncleaved{}".format(i)]: #determines relative cleavage
        if sequence in DATA["rz_cleaved{}".format(i)]:
            DATA["relative_cleavage{}".format(i)][sequence]=(DATA["rz_cleaved{}".format(i)][sequence]/(DATA["rz_uncleaved{}".format(i)][sequence]+DATA["rz_cleaved{}".format(i)][sequence]))
        elif sequence not in DATA["rz_cleaved{}".format(i)]:
            DATA["relative_cleavage{}".format(i)][sequence]=0
    for sequence in DATA["rz_cleaved{}".format(i)]:
        if sequence not in DATA["relative_cleavage{}".format(i)]:
            DATA["relative_cleavage{}".format(i)][sequence]=1
    print("Number of cleaved {} : ".format(i),len(DATA["rz_cleaved{}".format(i)]))
    print("Number of uncleaved {} : ".format(i),len(DATA["rz_uncleaved{}".format(i)]))
    print("Number of total {} : ".format(i),len(DATA["relative_cleavage{}".format(i)]))
#%%
pickle.dump(DATA['rz_cleaved1'], open("../Pickle/{}_cleaved.p".format(library), "wb"))
pickle.dump(DATA['rz_uncleaved1'], open("../Pickle/{}_uncleaved.p".format(library), "wb"))
pickle.dump(DATA['relative_cleavage1'], open("../Pickle/{}_relative_cleavage.p".format(library), "wb"))