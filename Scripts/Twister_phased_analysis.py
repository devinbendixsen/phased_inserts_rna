#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 21:38:04 2018

@author: devin
"""
# =============================================================================
# code to determine the correlation of read counts between using 25% and 0.5%PhiX
# =============================================================================
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats

sns.set(font_scale=2, font='bold',style='ticks')
sns.set_style('white')

#%% Upload and plot data of cleaved reads
Twister_05PhiX=pickle.load(open("../Pickle/Twister_phased_0.5_cleaved.p", "rb"))
Twister_25PhiX=pickle.load(open("../Pickle/Twister_phased_25_cleaved.p", "rb"))

sns.set(font_scale=2, font='bold')
xlist=[]
ylist=[]
for key in Twister_05PhiX:
    if Twister_05PhiX.get(key,0)>=0  and Twister_25PhiX.get(key,0)>0:
        x=(Twister_05PhiX.get(key,0))
        ylist.append(x)
        y=(Twister_25PhiX.get(key,0))
        xlist.append(y)
datas=pd.DataFrame({'0.5% PhiX':xlist,'25% PhiX':ylist})

g=sns.JointGrid(x='0.5% PhiX',y='25% PhiX',data=datas)
g = g.plot_joint(plt.scatter,color='b',edgecolor="black")
g = g.plot_marginals(sns.distplot, color="b")
g = g.plot_joint(sns.regplot, color='b')
plt.plot([0,45],[0,45],linestyle='dashed',linewidth=3,color='black')
plt.xticks([0,10,20,30,40])
plt.yticks([0,10,20,30,40])
plt.xlim(0,45)
plt.ylim(0,45)
slope, intercept, r_value, p_value, std_err = stats.linregress(xlist, ylist)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/Twister_Phased_correlation_cleaved.png',bbox_inches='tight',dpi=1000,tranparent=True)

#%% Upload and plot data of uncleaved reads
Twister_05PhiX=pickle.load(open("../Pickle/Twister_phased_0.5_uncleaved.p", "rb"))
Twister_25PhiX=pickle.load(open("../Pickle/Twister_phased_25_uncleaved.p", "rb"))

xlist=[]
ylist=[]
for key in Twister_05PhiX:
    if Twister_05PhiX.get(key,0)>=0  and Twister_25PhiX.get(key,0)>0:
        x=(Twister_05PhiX.get(key,0))
        ylist.append(x)
        y=(Twister_25PhiX.get(key,0))
        xlist.append(y)
datas=pd.DataFrame({'0.5% PhiX':xlist,'25% PhiX':ylist})

g=sns.JointGrid(x='0.5% PhiX',y='25% PhiX',data=datas)
g = g.plot_joint(plt.scatter, color='r',edgecolor="black")
g = g.plot_marginals(sns.distplot, color="r")
g = g.plot_joint(sns.regplot, color='r')
plt.plot([0,950],[0,950],linestyle='dashed',linewidth=3,color='black')
plt.xticks([0,200,400,600,800])
plt.yticks([0,200,400,600,800])
plt.xlim(0,900)
plt.ylim(0,900)
slope, intercept, r_value, p_value, std_err = stats.linregress(xlist, ylist)
print ('R^2 : ', r_value**2)
plt.savefig('../Figures/Twister_Phased_correlation_uncleaved.png',bbox_inches='tight',dpi=1000,tranparent=True)
    
#%% Upload and plot the relative cleavage
Twister_05PhiX=pickle.load(open("../Pickle/Twister_unphased_0.5_cleaved.p", "rb"))
Twister_25PhiX=pickle.load(open("../Pickle/Twister_phased_0.5_cleaved.p", "rb"))

xlist=[]
ylist=[]
for key in Twister_05PhiX:
    if Twister_05PhiX.get(key,0)>=0  and Twister_25PhiX.get(key,0)>0:
        x=(Twister_05PhiX.get(key,0))
        ylist.append(x)
        y=(Twister_25PhiX.get(key,0))
        xlist.append(y)
datas=pd.DataFrame({'0.5% PhiX':xlist,'25% PhiX':ylist})

g=sns.JointGrid(x='0.5% PhiX',y='25% PhiX',data=datas)
g = g.plot_joint(plt.scatter, color='gray',edgecolor="black")
g = g.plot_marginals(sns.distplot, color="gray")
g = g.plot_joint(sns.regplot, color='gray')
plt.plot([0,0.5],[0,0.5],linestyle='dashed',linewidth=3,color='black')
plt.xticks([0,0.1,0.2,0.3,0.4,0.5])
plt.yticks([0,0.1,0.2,0.3,0.4,0.5])
#plt.xlim(0,0.55)
#plt.ylim(0,0.55)
slope, intercept, r_value, p_value, std_err = stats.linregress(xlist, ylist)
print ('R^2 : ', r_value**2)
#plt.savefig('../Figures/Twister_Phased_correlation_relative_cleavage.png',bbox_inches='tight',dpi=1000,tranparent=True)
            
            
        