#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 15:54:27 2020

@author: devin
"""

# =============================================================================
# code to determine the correlation of read counts between using 25% and 0.5%PhiX
# =============================================================================
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

sns.set(font_scale=2, font='bold',style='ticks')
sns.set_style('ticks')
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 12}
plt.rc('font', **font)

#%%
def hide_current_axis(*args, **kwds):
    plt.gca().set_visible(False)
#%% TOTAL READS
P05_cleaved=pickle.load(open("../Pickle/Twister_phased_0.5_cleaved.p", "rb"))
P25_cleaved=pickle.load(open("../Pickle/Twister_phased_25_cleaved.p", "rb"))
U05_cleaved=pickle.load(open("../Pickle/Twister_unphased_0.5_cleaved.p", "rb"))
U25_cleaved=pickle.load(open("../Pickle/Twister_unphased_25_cleaved.p", "rb"))
P05_uncleaved=pickle.load(open("../Pickle/Twister_phased_0.5_uncleaved.p", "rb"))
P25_uncleaved=pickle.load(open("../Pickle/Twister_phased_25_uncleaved.p", "rb"))
U05_uncleaved=pickle.load(open("../Pickle/Twister_unphased_0.5_uncleaved.p", "rb"))
U25_uncleaved=pickle.load(open("../Pickle/Twister_unphased_25_uncleaved.p", "rb"))
P05_total={}
for seq in P05_uncleaved:
    P05_total[seq]=P05_uncleaved[seq]+P05_cleaved.get(seq,0)
P25_total={}
for seq in P25_uncleaved:
    P25_total[seq]=P25_uncleaved[seq]+P25_cleaved.get(seq,0)
U05_total={}
for seq in U05_uncleaved:
    U05_total[seq]=U05_uncleaved[seq]+U05_cleaved.get(seq,0)
U25_total={}
for seq in U25_uncleaved:
    U25_total[seq]=U25_uncleaved[seq]+U25_cleaved.get(seq,0)
data=pd.DataFrame()
data['U05']=pd.Series(U05_total)
data['U25']=pd.Series(U25_total)
data['P05']=pd.Series(P05_total)
data['P25']=pd.Series(P25_total)
plt.figure(figsize=[4.3,4.3])
g=sns.pairplot(data,diag_kws=dict(color='w',cut=0),diag_kind='kde',plot_kws=dict(color='gray',s=25,linewidth=0.5,alpha=0.5),markers='h')
g.map_upper(hide_current_axis)
plt.savefig('../Figures/Twister_Phased_total-read_correlation.svg',bbox_inches='tight',dpi=1000)

print('total reads')
print('U05:U25',(data['U05'].corr(data['U25']))**2)
print('U05:P05',(data['U05'].corr(data['P05']))**2)
print('U05:P25',(data['U05'].corr(data['P25']))**2)
print('U25:P05',(data['U25'].corr(data['P05']))**2)
print('U25:P25',(data['U25'].corr(data['P25']))**2)
print('P05:P25',(data['P05'].corr(data['P25']))**2)
#%%
x=['P05']*len(P05_total.values())
data1 = pd.DataFrame(list(zip(x,P05_total.values())), columns =['sample', 'count']) 
x=['P25']*len(P25_total.values())
data2 = pd.DataFrame(list(zip(x,P25_total.values())), columns =['sample', 'count'])
x=['U05']*len(U05_total.values())
data3 = pd.DataFrame(list(zip(x,U05_total.values())), columns =['sample', 'count'])
x=['U25']*len(U05_total.values())
data4 = pd.DataFrame(list(zip(x,U25_total.values())), columns =['sample', 'count'])
data=pd.concat([data3,data4,data1,data2])

g = sns.FacetGrid(data,row='sample',hue='sample',aspect=4, height=1.2, palette=['#b1d1fc','#fe7b7c','#01386a','#840000'])
g.map(sns.kdeplot, "count", clip_on=False, shade=True, alpha=1, lw=1.5, bw='scott',cut=0)
g.map(sns.kdeplot, "count", clip_on=False, color="w", lw=1.5, bw='scott',cut=0)
g.map(plt.axhline, y=0, lw=2, clip_on=False)
g.fig.subplots_adjust(hspace=-.5)
g.set_titles("")
g.set(yticks=[])
g.despine(bottom=True, left=True)
plt.xticks(fontsize=18)
plt.xlabel('reads per genotype',weight='bold',fontsize=20)
plt.savefig('../Figures/Twister_Phased_read-depth_total.png',bbox_inches='tight',dpi=1000,transparent=True)

print('mean')
print('P05',np.mean(list(P05_total.values())))
print('P25',np.mean(list(P25_total.values())))
print('U05',np.mean(list(U05_total.values())))
print('U25',np.mean(list(U25_total.values())))

print('min')
print('P05',np.min(list(P05_total.values())))
print('P25',np.min(list(P25_total.values())))
print('U05',np.min(list(U05_total.values())))
print('U25',np.min(list(U25_total.values())))

print('max')
print('P05',np.max(list(P05_total.values())))
print('P25',np.max(list(P25_total.values())))
print('U05',np.max(list(U05_total.values())))
print('U25',np.max(list(U25_total.values())))

print('total')
print('P05',sum(list(P05_total.values())))
print('P25',sum(list(P25_total.values())))
print('U05',sum(list(U05_total.values())))
print('U25',sum(list(U25_total.values())))