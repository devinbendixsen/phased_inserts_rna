#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 12:11:01 2018

@author: devin
"""
# =============================================================================
# Determine and plot the nucleotide diversity and entropy (bits) of the simulated
# sequencing run using phased inserts
# =============================================================================
import matplotlib.pyplot as plt
from random import randint
import numpy as np
Twister = 'AACACTGCCAATGCCGGTNNNAAGCCCGGATAAAAGTGGANNNGGCGG'
from itertools import product

#%%generates and populates the 4096 sequences in the TwisterLIB6 library
sequences=[]
dict={1:['GGGATGCATGCATGCGGGGGAAAAGGGCCGCCTAACACTGCCAATGCCGGT'], 2:['A','C','G','T'], 3:['A','C','G','T'], 4:['A','C','G','T'], 5:['AAGCCCGGATAAAAGTGGA'], 6:['A','C','G','T'], 7:['A','C','G','T'], 8:['A','C','G','T'], 9:['GGCGG'],10:['TCGATCCGGTTCGCCGGACCAAATCGGGCTTCGGTCCGGTTCCTGTCTCTTATACACATCTCCGAGCCCACGAATCG']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10]):
    sequences.append(x)
TwisterLIB6=[]

for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])
    TwisterLIB6.append(seq)

platesize=1000000
plate =[]
while len(plate)<platesize:
    x=TwisterLIB6[randint(1, len(TwisterLIB6)-1)]
    plate.append(x)

diversity={}
for i in range(0,151):
    diversity[i]=[]
    for j in plate:
        diversity[i].append(j[i])

#%% Calculates entropy 
entropy1=[]
plt.figure(1,figsize=[12,8])
plt.subplot(411)
for i in diversity:
    A=(diversity[i].count('A')+1)/len(diversity[i])
    C=(diversity[i].count('C')+1)/len(diversity[i])
    T=(diversity[i].count('T')+1)/len(diversity[i])
    G=(diversity[i].count('G')+1)/len(diversity[i])
    entropy=(-(A*np.log2(A))-(C*np.log2(C))-(T*np.log2(T))-(G*np.log2(G)))
    entropy1.append(entropy)
    if A>0:
        plt.scatter(i,A,c='steelblue',edgecolor='black',s=50, marker='o')
    if C>0:
        plt.scatter(i,C,c='lemonchiffon',edgecolor='black',s=50, marker='o')
    if T>0:
        plt.scatter(i,T,c='salmon',edgecolor='black',s=50, marker='o')
    if G>0:
        plt.scatter(i,G,c='lightgreen',edgecolor='black',s=50, marker='o')
plt.yticks([0,00,0.25,0.50,0.75,1],weight='bold',fontsize=15,color='black')
plt.xticks([],[])
plt.ylim(-0.05,1.05)
plt.xlim(0,150)

#%%
sequences=[]
dict={1:['GCATGCATGCATGCATGC','TGCATGCATGCATGC','ATGCATGCATGC','CATGCATGC'],2:['GGGATGCATGCATGCGGGGGAAAAGGGCCGCCTAACACTGCCAATGCCGGT'], 3:['A','C','G','T'], 4:['A','C','G','T'], 5:['A','C','G','T'], 6:['AAGCCCGGATAAAAGTGGA'], 7:['A','C','G','T'], 8:['A','C','G','T'], 9:['A','C','G','T'], 10:['GGCGGTCGATCCGGTTCGCCGGACCAAATCGGGCTTCGGTCCGGTTCCTGTCTCTTATACACATCTCCGAGCCCACGAATCGATC']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10]):
    sequences.append(x)
TwisterLIB6phased=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])
    seq=seq[0:151]
    TwisterLIB6phased.append(seq)
#%%
print(len(TwisterLIB6phased))
plate =[]
while len(plate)<platesize:
    x=TwisterLIB6phased[randint(1, len(TwisterLIB6phased)-1)]
    plate.append(x)

diversity={}
for i in range(0,151):
    diversity[i]=[]
    for j in plate:
        diversity[i].append(j[i])

#%%
entropy3=[]
plt.subplot(413)
for i in diversity:
    A=(diversity[i].count('A')+1)/len(diversity[i])
    C=(diversity[i].count('C')+1)/len(diversity[i])
    T=(diversity[i].count('T')+1)/len(diversity[i])
    G=(diversity[i].count('G')+1)/len(diversity[i])
    entropy=(-(A*np.log2(A))-(C*np.log2(C))-(T*np.log2(T))-(G*np.log2(G)))
    entropy3.append(entropy)
    if A>0:
        plt.scatter(i,A,c='steelblue',edgecolor='black',s=50, marker='o')
    if C>0:
        plt.scatter(i,C,c='lemonchiffon',edgecolor='black',s=50, marker='o')
    if T>0:
        plt.scatter(i,T,c='salmon',edgecolor='black',s=50,marker='o')
    if G>0:
        plt.scatter(i,G,c='lightgreen',edgecolor='black',s=50, marker='o')
plt.yticks([0,00,0.25,0.50,0.75,1],weight='bold',fontsize=15,color='black')
plt.xticks([],[])
plt.ylim(-0.05,1.05)
plt.xlim(0,150)
#%%

sequences=[]
dict={1:['GCATGCATGCATGCATGC','TGCATGCATGCATGC','ATGCATGCATGC','CATGCATGC'],2:['GGGATGCATGCATGCGGGGGAAAAGGGCCGCCTAACACTGCCAATGCCGGT'], 3:['A','C','G','T'], 4:['A','C','G','T'], 5:['A','C','G','T'], 6:['AAGCCCGGATAAAAGTGGA'], 7:['A','C','G','T'], 8:['A','C','G','T'], 9:['A','C','G','T'], 10:['GGCGGTCGATCCGGTTCGCCGGACCAAATCGGGCTTCGGTCCGGTTCCTGTCTCTTATACACATCTCCGAGCCCACGAATCGATC']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10]):
    sequences.append(x)
TwisterLIB6phased=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])
    seq=seq[0:151]
    TwisterLIB6phased.append(seq)
    
#%%
print(len(TwisterLIB6phased))
plate =[]
while len(plate)<(0.75*platesize):
    x=TwisterLIB6phased[randint(1, len(TwisterLIB6phased)-1)]
    plate.append(x)
    
import string
import random
def id_generator(size=151, chars=['A','C','T','G']):
    return ''.join(random.choice(chars) for _ in range(size))
while len(plate)<platesize:
    y=id_generator()
    plate.append(y)

diversity={}
for i in range(0,151):
    diversity[i]=[]
    for j in plate:
        diversity[i].append(j[i])

#%%
entropy4=[]
plt.subplot(414)
for i in diversity:
    A=(diversity[i].count('A')+1)/len(diversity[i])
    C=(diversity[i].count('C')+1)/len(diversity[i])
    T=(diversity[i].count('T')+1)/len(diversity[i])
    G=(diversity[i].count('G')+1)/len(diversity[i])
    entropy=(-(A*np.log2(A))-(C*np.log2(C))-(T*np.log2(T))-(G*np.log2(G)))
    entropy4.append(entropy)
    if A>0:
        plt.scatter(i,A,c='steelblue',edgecolor='black',s=50, marker='o')
    if C>0:
        plt.scatter(i,C,c='lemonchiffon',edgecolor='black',s=50, marker='o')
    if T>0:
        plt.scatter(i,T,c='salmon',edgecolor='black',s=50, marker='o')
    if G>0:
        plt.scatter(i,G,c='lightgreen',edgecolor='black',s=50, marker='o')
plt.xlim(0,150)
plt.yticks([0,00,0.25,0.50,0.75,1],weight='bold',fontsize=15,color='black')
plt.xticks(weight='bold',fontsize=15,color='black')
plt.ylim(-0.05,1.05)

#%%
sequences=[]
dict={1:['GGGATGCATGCATGCGGGGGAAAAGGGCCGCCTAACACTGCCAATGCCGGT'], 2:['A','C','G','T'], 3:['A','C','G','T'], 4:['A','C','G','T'], 5:['AAGCCCGGATAAAAGTGGA'], 6:['A','C','G','T'], 7:['A','C','G','T'], 8:['A','C','G','T'], 9:['GGCGG'],10:['TCGATCCGGTTCGCCGGACCAAATCGGGCTTCGGTCCGGTTCCTGTCTCTTATACACATCTCCGAGCCCACGAATCGATC']}
for x in product(dict[1], dict[2], dict[3], dict[4], dict[5], dict[6], dict[7], dict[8], dict[9], dict[10]):
    sequences.append(x)
TwisterLIB6=[]
for x in sequences:
    seq=str(x[0])+str(x[1])+str(x[2])+str(x[3])+str(x[4])+str(x[5])+str(x[6])+str(x[7])+str(x[8])+str(x[9])
    seq=seq[0:151]
    TwisterLIB6.append(seq)
print(len(TwisterLIB6))
platesize=1000000
plate =[]
while len(plate)<(0.75*platesize):
    x=TwisterLIB6[randint(1, len(TwisterLIB6)-1)]
    plate.append(x)
while len(plate)<platesize:
    y=id_generator()
    plate.append(y)
diversity={}
for i in range(0,151):
    diversity[i]=[]
    for j in plate:
        diversity[i].append(j[i])

#%%
entropy2=[]
plt.subplot(412)
for i in diversity:
    A=(diversity[i].count('A')+1)/len(diversity[i])
    C=(diversity[i].count('C')+1)/len(diversity[i])
    T=(diversity[i].count('T')+1)/len(diversity[i])
    G=(diversity[i].count('G')+1)/len(diversity[i])
    entropy=(-(A*np.log2(A))-(C*np.log2(C))-(T*np.log2(T))-(G*np.log2(G)))
    entropy2.append(entropy)
    if A>0:
        plt.scatter(i,A,c='steelblue',edgecolor='black',s=50, marker='o')
    if C>0:
        plt.scatter(i,C,c='lemonchiffon',edgecolor='black',s=50, marker='o')
    if T>0:
        plt.scatter(i,T,c='salmon',edgecolor='black',s=50, marker='o')
    if G>0:
        plt.scatter(i,G,c='lightgreen',edgecolor='black',s=50, marker='o')
plt.xlim(0,150)
plt.yticks([0,00,0.25,0.50,0.75,1],weight='bold',fontsize=15,color='black')
plt.xticks([],[])
plt.ylim(-0.05,1.05)
plt.tight_layout()
plt.savefig('../Figures/TwisterLIB6Phased_model2.png',bbox_inches='tight',dpi=1000,transparent=True)

#%%
import seaborn as sns
sns.set_style('ticks')
plt.figure(2,figsize=[12,4])
plt.plot(range(0,151),entropy1,color='#b1d1fc',zorder=0,linewidth=2)
plt.plot(range(0,151),entropy2,color='#fe7b7c',zorder=0,linewidth=2)
plt.plot(range(0,151),entropy3,color='#01386a',zorder=0,linewidth=2)
plt.plot(range(0,151),entropy4,color='#840000',zorder=0,linewidth=2)
s=25
plt.scatter(range(0,151),entropy1,color='#b1d1fc',edgecolor='black',linewidth=1,s=s)
plt.scatter(range(0,151),entropy2,color='#fe7b7c',edgecolor='black',linewidth=1,s=s)
plt.scatter(range(0,151),entropy3,color='#01386a',edgecolor='black',linewidth=1,s=s)
plt.scatter(range(0,151),entropy4,color='#840000',edgecolor='black',linewidth=1,s=s)
plt.xlim(0,150)
plt.xticks(weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.ylabel('entropy (bits)',weight='bold',fontsize=15,color='black')
plt.xlabel('position in read (bp)',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/TwisterLIB6Phased_model_entropy.png',bbox_inches='tight',dpi=1000,transparent=False)

#%%
print('entropy1',np.mean(entropy1),np.std(entropy1))
print('entropy2',np.mean(entropy2),np.std(entropy2))
print('entropy3',np.mean(entropy3),np.std(entropy3))
print('entropy4',np.mean(entropy4),np.std(entropy4))