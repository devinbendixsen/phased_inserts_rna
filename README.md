# Phased_Inserts_RNA

Bendixsen et al. (in review) Python data (.p files) and scripts used for data analysis of high-throughput sequencing data. Data depicts the usage of phased nucleotide inserts for sequencing low-diversity RNA samples.

## Installation

The directory is formatted as spyder python project.


## Usage

Python scripts in this directory encompass two categories:\
**(1)** Scripts used to analyze raw sequencing data of samples sequenced with 25% PhiX or 0.5% PhiX.\
**(2)** Scripts that analyzed data to allow us to compare our samples to each other and previously published measurements (Kobori et al. 2016)

    
Data in this directory encompass two categories:\
**(1)** Excel spreadsheets (.xlsx)\
**(2)** Python pickle files (.p) that contain calculated metrics.
    
Figures presented in Bendixsen et al. (in review) are found in the Figures directory.

## Authors and acknowledgment

Python scripts were developed by **Devin Bendixsen, PhD** in collaboration with **Eric Hayden, PhD**.